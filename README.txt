
Relevant Content Location - Sandbox Project

INSTALLATION
============

See Drupal module installation guidelines:
http://drupal.org/getting-started/install-contrib/modules

Prerequisite: This module requires location. You must first install that module
from: http://drupal.org/project/location

1. Unpack module to your /sites/all/modules directory.
2. Visit your module management page at /admin/build/modules. Enable Relevant
Content Location. The Location module is also required.
3. Configure the module via /admin/settings/relevant_content_location. At the
very least you have to check your content types settings.

HELP AND ASSISTANCE
===================

Help is available in the issue queue. If you are asking for help, please
provide the following information with your request:

- Version of Drupal you are using, or Pressflow version if you are using that
- The browsers you are testing with and version (Firefox 3.6.10, IE8, etc.)
- Any relevant information from the Firebug console or a similar Javascript
  debugger
- Screenshots or screencast videos showing your errors are helpful
- Any additional details that the module authors can use to reproduce this
	with a default installation of Drupal and Location.


---
Created by Florian Margaine, aka Ralt
