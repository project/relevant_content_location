<?php
/**
 * @file
 * Relevant Content Location admin settings form.
 */

/**
 * Implements hook_admin().
 */
function relevant_content_location_admin() {
  $form = array();

  $form['general_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['general_settings']['block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title of the block'),
    '#description' => t('Set the title you want to use for the block'),
    '#default_value' => variable_get('relevant_content_location_block_title'),
    '#size' => 30,
  );

  $form['general_settings']['relevant_content_location_items_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of items you want to show in the block'),
    '#description' => t('Set the number of items you want to show in the block'),
    '#default_value' => variable_get('relevant_content_location_items_number', 5),
    '#size' => 10,
  );

  $form ['general_settings']['relevant_content_location_theme'] = array(
    '#type' => 'radios',
    '#title' => t('Output used'),
    '#description' => t('Select which output to use'),
    '#default_value' => variable_get('relevant_content_location_theme', 'item_list'),
    '#options' => array(
      'item_list' => t('HTML List'),
      'table' => t('HTML Table'),
    ),
  );

  $form['general_settings']['relevant_content_location_location_field'] = array(
    '#type' => 'radios',
    '#title' => t("Location's field used"),
    '#description' => t("Which location's field do you want to use to find your relevant content?"),
    '#default_value' => variable_get('relevant_content_location_location_field', 'country'),
    '#options' => array(
      'street' => t('Street'),
      'additional' => t('Additional'),
      'city' => t('City'),
      'province' => t('Province'),
      'postal_code' => t('Postal code'),
      'country' => t('Country'),
    ),
  );

  $form['general_settings']['relevant_content_location_sorting'] = array(
    '#type' => 'radios',
    '#title' => t('Results sorting'),
    '#description' => t('How do you want to sort your results?'),
    '#default_value' => variable_get('relevant_content_location_sorting','random'),
    '#options' => array(
      'random' => t('Randomly'),
      'last_modified' => t('Last modified'),
      'first_modified' => t('First modified'),
      'last_created' => t('Last created'),
      'first_created' => t('First created'),
    )
  );

  $form['content_types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content types'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  // Get all node types
  $types = array_map('check_plain', node_get_types('names'));
  foreach ($types as $key => $value) {
    $form['content_types']['relevant_content_location_' . $key] = array(
      '#type' => 'checkboxes',
      '#title' => t('Relevant content for ') . $value,
      '#description' => t('Select the content types to show on this content type\'s nodes.'),
      '#default_value' => variable_get('relevant_content_location_' . $key, array()),
      '#options' => $types,
    );
  }

  $form['views_pages'] = array(
    '#type' => 'fieldset',
    '#title' => t('Views pages'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['views_pages']['relevant_content_location_callback'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of the filter of the view'),
    '#description' => t('On views pages, relevant content is based on exposed filters. Set the name of the filter used in the URI here; example : if your URI is "http://www.yoursite.com/views?country=us", then your filter is "country".'),
    '#default_value' => variable_get('relevant_content_location_callback', 'country'),
    '#size' => 60,
  );

  $form['views_pages']['relevant_content_location_callback_argument'] = array(
    '#type' => 'textfield',
    '#title' => t('Value of the argument when it is "All"'),
    '#description' => t('If set, what is the value of the argument when it\'s displaying everything?'),
    '#default_value' => variable_get('relevant_content_location_callback_argument', 'All'),
    '#size' => 60,
  );

  $form['views_pages']['relevant_content_location_views_content_type'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Relevant content for the views'),
    '#description' => t('Select the content types to show on the views.'),
    '#default_value' => variable_get('relevant_content_location_views_content_type', array()),
    '#options' => $types,
  );

  return system_settings_form($form);
}

/**
 * Validate the settings form.
 */
function relevant_content_location_admin_settings_validate($form, $form_state) {
  $items_number = $form_state['values']['relevant_content_location_items_number'];
  if (!is_numeric($items_number)) {
    form_set_error('relevant_content_location_items_number', t('Please enter a number.'));
  }
}
